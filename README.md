# Django Classified Compose File

## Getting started

This is a test for solving an issue with Telegram login using AllAuth plugin for django.

 * You need [docker compose](https://docs.docker.com/compose/install/) installled.
 * You have to have `.env` and `nginx-certbot.env` files setup correctly to run it. templates are availbe in project directory.

## Setup environment

```
mv env.template .env
mv nginx-certbot.env.template nginx-certbot.env
```

Modify environment files to your need. Currently it's configured to run on localhost, Telegram login is activated if you set Telegram token and Bot name.

## Run it

```
docker compose up
```

I've used instructions from [ JonasAlfredsson /
docker-nginx-certbot](https://github.com/JonasAlfredsson/docker-nginx-certbot#run-with-docker-compose) and  [
awesome-compose/official-documentation-samples/django/
](https://github.com/docker/awesome-compose/tree/master/official-documentation-samples/django/ )
 to create it.

 
