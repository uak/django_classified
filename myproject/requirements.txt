Django>=4.0.8
psycopg2>=2.8
django-classified>=1.0
django-allauth>=0.52.0
gunicorn>=20.1.0
