server {
    # Listen to port 443 on both IPv4 and IPv6.
    listen 443 ssl default_server reuseport;
    listen [::]:443 ssl default_server reuseport;

    # Domain names this server should respond to.
    server_name localhost;

    # Load the certificate files.
    ssl_certificate         /etc/letsencrypt/live/test03/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/test03/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/test03/chain.pem;

    # Load the Diffie-Hellman parameter.
    ssl_dhparam /etc/letsencrypt/dhparams/dhparam.pem;

    charset utf-8;

    proxy_connect_timeout 600;
    proxy_send_timeout 600;
    proxy_read_timeout 600;
    send_timeout 600;


    # max upload size
    client_max_body_size 512M;  


    location /static {
        alias /storage/static;
    }

    location /media {
        alias /storage/media;
    }

    location / {
        proxy_pass http://web:80;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

}
